import random
from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
import st3m.run
import leds


class ReactionThingy(Application):
    COLORS = [
        [0.5, 0.0, 1.0], # purple
        [0.0, 0.0, 0.9], # blue
        [0.0, 0.5, 1.0], # sky
        [0.0, 0.9, 0.9], # cyan
        [0.0, 1.0, 0.5], # teal
        [0.0, 0.9, 0.0], # green
        [0.5, 1.0, 0.0], # lime
        [0.9, 0.9, 0.0], # yellow
        [1.0, 0.5, 0.0], # orange
        [0.9, 0.0, 0.0], # red
    ]

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.countDown = random.randint(1000, 5000)
        self.winner = None

    def draw(self, ctx: Context) -> None:
        if self.countDown <= 0:
            if self.winner is not None:
                # someone has won a round (colored)
                rgb = self.COLORS[self.winner]
                ctx.rgb(rgb[0], rgb[1], rgb[2]).rectangle(-120, -120, 240, 240).fill()
            else:
                # go go go (white)
                ctx.rgb(1, 1, 1).rectangle(-120, -120, 240, 240).fill()
        else:
            # idling (black)
            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.countDown > 0:
            self.countDown -= delta_ms
        elif not self.winner is not None:
            pressed_petals = [(i, p.pressure) for i, p in
                              enumerate(self.input.captouch.petals)
                              if p.pressure > 0]
            if pressed_petals:
                self.winner = max(pressed_petals, key=lambda x: x[1])[0]
                print(f"winner: {self.winner}")


if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(ReactionThingy(ApplicationContext()))
